

export default class Healing extends createjs.Sprite {

    constructor(chargeur, conteneurEnnemis = new createjs.Container, indexEnnemi, leJeu, healer) {

        super(chargeur.getResult("healing"));

        this.gotoAndPlay("Fx_effect17-2");

        createjs.Sound.play("heal", {"volume": 0.3});

        this.indexEnnemi = indexEnnemi;
        this.conteneurEnnemis = conteneurEnnemis;

        this.leJeu = leJeu;
        this.healer = healer;

        this.scaleX = 2;
        this.scaleY = 2;


        setTimeout(this.detruire.bind(this), 800);
        setTimeout(this.dommage.bind(this), 300);


    }

    //-----------------------------------------------------------------------------------------

    dommage(){

        this.leJeu.attaquant.vie += 3;
        if(this.leJeu.attaquant.vie > 15)
        {
            this.leJeu.attaquant.vie = 15;
            this.leJeu.attaquant.barreDeVie.updateMonCache(this.leJeu.attaquant.vie);

        }
        else
        {
            this.leJeu.attaquant.barreDeVie.updateMonCache(this.leJeu.attaquant.vie);

        }

        this.healer.vie += 3;
        if(this.healer.vie > 15)
        {
            this.healer.vie = 15;
            this.healer.barreDeVie.updateMonCache(this.healer.vie);

        }
        else
        {
            this.healer.barreDeVie.updateMonCache(this.healer.vie);

        }
    }

    detruire(){
        let dommageAttaque = Math.random() + 1;
        let dommageAttaqueArrondi = Math.round(dommageAttaque);

        this.conteneurEnnemis.getChildAt(this.indexEnnemi).vie -= dommageAttaqueArrondi;
        this.conteneurEnnemis.getChildAt(this.indexEnnemi).hit();
        this.conteneurEnnemis.getChildAt(this.indexEnnemi).die();
        this.parent.removeChild(this);
    }
   //detruire(){

   //    this.parent.removeChild(this);

   //}
}