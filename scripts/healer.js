import Healing from './healing.js';
import AttackHealer from './AttackHealer.js';
import BarreDeVie from "./barreDeVie.js";

export default class Healer extends createjs.Sprite {

    constructor(chargeur, conteneurEnnemis = new createjs.Container, leJeu) {

        super(chargeur.getResult("healer"));

        this.gotoAndPlay("Anim_char3_idle");

        this.leChargeur = chargeur;

        this.leJeu = leJeu;

        this.vie = 15;



        this.conteneurEnnemis = conteneurEnnemis;


    }

    //-----------------------------------------------------------------------------------------


    //-----------------------------------------------------------------------------------------

    //--- Heal ---//
    attack2(indexEnnemi) {

        this.gotoAndPlay("Anim_char3_attack");
        setTimeout(() => this.gotoAndPlay("Anim_char3_idle"), 800);

        this.healing = new Healing(this.leChargeur, this.conteneurEnnemis, indexEnnemi, this.leJeu, this);
        this.healing.x = 250;
        this.healing.y = 300;

        let index = this.stage.getChildIndex(this);
        this.stage.addChildAt(this.healing, index);


    }

    //-----------------------------------------------------------------------------------------

    attack1(indexEnnemi) {

        this.gotoAndPlay("Anim_char3_attack");
        setTimeout(() => this.gotoAndPlay("Anim_char3_idle"), 800);

        this.attack = new AttackHealer(this.leChargeur, this.conteneurEnnemis, this.x, this.y, indexEnnemi);
        this.attack.x = this.x;
        this.attack.y = this.y;

        let index = this.stage.getChildIndex(this);
        this.stage.addChildAt(this.attack, index);


    }

    hit()
    {
        this.gotoAndPlay("Anim_char3_gethit");
        setTimeout(() => this.gotoAndPlay("Anim_char3_idle"), 800);
        this.barreDeVie.updateMonCache(this.vie);
    }

    die(){
        if (this.vie <= 0)
        {
            createjs.Sound.play("hurt", {"volume": 1});
            this.leJeu.texteDefaite();
            this.stage.removeChild(this.barreDeVie);
            console.log("Healer mort");
            this.parent.removeChild(this)
        }
    }

    afficherVie(x, y){
        this.barreDeVie = new BarreDeVie(this.vie);
        this.barreDeVie.x = x + this.getBounds().width/2 ;
        this.barreDeVie.y = y + this.getBounds().height/2 - 15;

        this.stage.addChild(this.barreDeVie);
    }

}