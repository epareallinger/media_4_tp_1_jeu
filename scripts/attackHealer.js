export default class AttackHealer extends createjs.Sprite {

  constructor(chargeur, conteneurEnnemis = new createjs.Container , valeurX, valeurY, indexEnnemi) {

      super(chargeur.getResult("healerAttack"));

      this.gotoAndPlay("Fx_effect1");

      createjs.Sound.play("vent", {"volume": 0.3});

      this.conteneurEnnemis = conteneurEnnemis;
      this.indexEnnemi = indexEnnemi;

      this.valeurX = valeurX;
      this.valeurY = valeurY;

      this.x = this.valeurX;
      this.y = this.valeurY;

      this.ennemiChoix();
  }

  ennemiChoix(){
      createjs.Tween
                .get(this, createjs.Ease.linear)
                .to({x: this.conteneurEnnemis.getChildAt(this.indexEnnemi).x - 100 -  this.conteneurEnnemis.getChildAt(this.indexEnnemi).ajustementX, y: this.conteneurEnnemis.getChildAt(this.indexEnnemi).y + 50 + this.conteneurEnnemis.getChildAt(this.indexEnnemi).ajustementX}, 1000)
                .call(this.detruire.bind(this));


  }

  detruire(){
      this.parent.removeChild(this);
            createjs.Tween.removeTweens(this);

      let dommageAttaque = Math.random()*1.99 + 1;
      let dommageAttaqueArrondi = Math.floor(dommageAttaque);

      this.conteneurEnnemis.getChildAt(this.indexEnnemi).vie -= dommageAttaqueArrondi;
      this.conteneurEnnemis.getChildAt(this.indexEnnemi).hit();
      this.conteneurEnnemis.getChildAt(this.indexEnnemi).die();
  }




 //  ennemiChoix(){
 //      if(this.indexEnnemi === 0)
 //      {
 // //         this.ennemi1();
 //      }
 //      else if(this.indexEnnemi === 1)
 //      {
 //          this.ennemi2();
 //      }
 //      else if(this.indexEnnemi === 2)
 //      {
 //          this.ennemi3();
 //      }
 //  }

 //  ennemi1(){

 //      createjs.Tween
 //          .get(this, createjs.Ease.linear)
 //          .to({x: this.conteneurEnnemis.getChildAt(0).x - 100, y: this.conteneurEnnemis.getChildAt(0).y + 50}, 1000)
 //          .call(this.detruire1.bind(this));

 //  }

 //  ennemi2(){

 //      createjs.Tween
 //          .get(this, createjs.Ease.linear)
 //          .to({x: this.conteneurEnnemis.getChildAt(1).x - 100, y: this.conteneurEnnemis.getChildAt(1).y + 50}, 1000)
 //          .call(this.detruire2.bind(this));

 //  }

 //  ennemi3(){

 //      createjs.Tween
 //          .get(this, createjs.Ease.linear)
 //          .to({x: this.conteneurEnnemis.getChildAt(2).x - 100, y: this.conteneurEnnemis.getChildAt(2).y + 50}, 1000)
 //          .call(this.detruire3.bind(this));

 //  }

 //  detruire1(){

 //      this.parent.removeChild(this);
 //      createjs.Tween.removeTweens(this);
 //      this.conteneurEnnemis.getChildAt(0).hit1();

 //  }

 //  detruire2(){

 //      this.parent.removeChild(this);
 //      createjs.Tween.removeTweens(this);
 //      this.conteneurEnnemis.getChildAt(1).hit2();

 //  }

 //  detruire3(){

 //      this.parent.removeChild(this);
 //      createjs.Tween.removeTweens(this);
 //      this.conteneurEnnemis.getChildAt(2).hit3();

 //  }


}
