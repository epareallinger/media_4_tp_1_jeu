
import AttackSkeleton from "./attackSkeleton.js";
import BarreDeVieRouge from "./barreDeVieRouge.js";


export default class Skeleton extends createjs.Sprite {

    constructor(chargeur, conteneurEnnemis = new createjs.Container, leJeu) {

        super(chargeur.getResult("skeleton"));


        this.leChargeur = chargeur;
        this.conteneurEnnemis = conteneurEnnemis;
        this.leJeu = leJeu;
        this.vie = 10;
        this.ajustementX = 0;
        this.ajustementY = 0;

        this.gotoAndPlay("Anim_char12_idle");


    }

    attack(healer, attaquant){
        this.gotoAndPlay("Anim_char12_attack");
        setTimeout(() => this.gotoAndPlay("Anim_char12_idle"), 800);

        let choixHeros = Math.random();
        let choixHerosArrondi = Math.round(choixHeros);

        let dommageAttaque = Math.random() + 2;
        let dommageAttaqueArrondi = Math.round(dommageAttaque);

        this.attackEnnemi = new AttackSkeleton(this.leChargeur, this.conteneurEnnemis, this.leJeu);

        if(choixHerosArrondi === 0)
        {
            this.attackEnnemi.x = attaquant.x;
            this.attackEnnemi.y = attaquant.y;

            let index = this.stage.getChildIndex(attaquant);
            this.stage.addChildAt(this.attackEnnemi, index + 1);

            attaquant.vie -= dommageAttaqueArrondi;
            attaquant.hit();
            attaquant.die();

        }
        else if (choixHerosArrondi === 1)
        {
            this.attackEnnemi.x = healer.x;
            this.attackEnnemi.y = healer.y;

            let index = this.stage.getChildIndex(healer);
            this.stage.addChildAt(this.attackEnnemi, index + 1);

            healer.vie -= dommageAttaqueArrondi;
            healer.hit();
            healer.die();
        }
    }

    //-------------//

    afficherVie(x, y){
        this.barreDeVie = new BarreDeVieRouge(this.vie);
        this.barreDeVie.x = x -this.getBounds().width -15;
        this.barreDeVie.y = y + this.getBounds().height - 15;

        this.stage.addChild(this.barreDeVie);
    }

    //-------------//

    hit(){


        this.gotoAndPlay("Anim_char12_gethit");
        setTimeout(this.return.bind(this), 500);
        this.barreDeVie.updateMonCache(this.vie);

    }

    return(){

        this.gotoAndPlay("Anim_char12_idle");

    }

    die(){
        if(this.vie<=0)
        {
            this.stage.removeChild(this.barreDeVie);
            createjs.Tween
                .get(this, createjs.Ease.linear)
                .to({alpha: 0 }, 1)
                .wait(100)
                .to({alpha: 1 }, 1)
                .wait(100)
                .to({alpha: 0 }, 1)
                .wait(100)
                .to({alpha: 1 }, 1)
                .wait(100)
                .to({alpha: 0 }, 1)
                .wait(100)
                .to({alpha: 1 }, 1)
                .call(function(){
                    this.parent.removeChild(this);
                })
        }
    }



}