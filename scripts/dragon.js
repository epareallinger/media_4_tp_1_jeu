import BarreDeVieRouge from "./barreDeVieRouge.js";
import AttackDragon from "./attackDragon.js";


export default class Dragon extends createjs.Sprite {

    constructor(chargeur, conteneurEnnemis = new createjs.Container, leJeu) {

        super(chargeur.getResult("dragon"));

        createjs.Sound.play("dragonSon", {"volume": 0.8});

        this.gotoAndPlay("Anim_char12_idle");

        this.scaleX = -3;
        this.scaleY = 3;
        this.x = 1700;
        this.y = 100;
        this.ajustementX = 120;
        this.ajustementY = 170;

        this.vie = 20;

        this.leChargeur = chargeur;
        this.conteneurEnnemis = conteneurEnnemis;
        this.leJeu = leJeu;

        this.arrive();

    }

    arrive(){
        createjs.Tween
            .get(this, createjs.Ease.linear)
            .to({x: 1050}, 3000)
    }

    afficherVie(x, y){
        this.barreDeVie = new BarreDeVieRouge(this.vie);
        this.barreDeVie.x = x - this.getBounds().width*2.8;
        this.barreDeVie.y = y + this.getBounds().height*4;

        this.stage.addChild(this.barreDeVie);
    }



    attack(healer, attaquant){
        this.gotoAndPlay("Anim_char12_attack");
        this.healer = healer;
        setTimeout(() => this.gotoAndPlay("Anim_char12_idle"), 800);

        this.attackEnnemi = new AttackDragon(this.leChargeur, this.conteneurEnnemis, this.leJeu, this, healer, attaquant);

            let index = this.stage.getChildIndex(attaquant);
            this.stage.addChildAt(this.attackEnnemi, index + 2);


    }




    hit(){

        this.gotoAndPlay("Anim_char12_gethit");
        setTimeout(this.return.bind(this), 500);
        this.barreDeVie.updateMonCache(this.vie);

    }

    die(){
        if(this.vie<=0)
        {
            this.stage.removeChild(this.barreDeVie);
            createjs.Tween
                .get(this, createjs.Ease.linear)
                .to({alpha: 0 }, 1)
                .wait(100)
                .to({alpha: 1 }, 1)
                .wait(100)
                .to({alpha: 0 }, 1)
                .wait(100)
                .to({alpha: 1 }, 1)
                .wait(100)
                .to({alpha: 0 }, 1)
                .wait(100)
                .to({alpha: 1 }, 1)
                .call(function(){this.parent.removeChild(this)})
            this.leJeu.texteVictoire();

        }
    }

    return(){

        this.gotoAndPlay("Anim_char12_idle");

    }



}