import AttackArcher1 from "./attackArcher1.js";
import AttackArcher2 from "./attackArcher2.js";
import BarreDeVie from "./barreDeVie.js";

export default class Archer extends createjs.Sprite {

    constructor(chargeur, conteneurEnnemis = new createjs.Container, leJeu) {

        super(chargeur.getResult("archer"));

        this.gotoAndPlay("Anim_char2_idle");
        this.leChargeur = chargeur;

        this.leJeu = leJeu;

        this.vie = 15;

        this.conteneurEnnemis = conteneurEnnemis;


    }


    //-----------------------------------------------------------------------------------------

    attack1(indexEnnemi) {
        this.gotoAndPlay("Anim_char2_attack");
        setTimeout(() => this.gotoAndPlay("Anim_char2_idle"), 800);
        this.attacker1 = new AttackArcher1(this.leChargeur, this.conteneurEnnemis, this.x, this.y, indexEnnemi);
        this.attacker1.x = 600;
        this.attacker1.y = 225;

        let index = this.stage.getChildIndex(this);
        this.stage.addChildAt(this.attacker1, index);


    }

    //-----------------------------------------------------------------------------------------

    attack2(indexEnnemi) {
        this.gotoAndPlay("Anim_char2_attack");
        setTimeout(() => this.gotoAndPlay("Anim_char2_idle"), 800);

        this.attacker2 = new AttackArcher2(this.leChargeur, this.conteneurEnnemis, this.x, this.y, indexEnnemi);

        let index = this.stage.getChildIndex(this);
        this.stage.addChildAt(this.attacker2, index);


    }

    //--------------------------//

    afficherVie(x, y){
        this.barreDeVie = new BarreDeVie(this.vie);
        this.barreDeVie.x = x + this.getBounds().width/2 -5;
        this.barreDeVie.y = y + this.getBounds().height/2;

        this.stage.addChild(this.barreDeVie);
    }
    //--------------------------//
    hit()
    {
        this.gotoAndPlay("Anim_char2_gethit");
        setTimeout(() => this.gotoAndPlay("Anim_char2_idle"), 800);
        this.barreDeVie.updateMonCache(this.vie);
    }

    die(){
        if(this.vie <= 0)
        {
            createjs.Sound.play("hurt", {"volume": 1});
            this.leJeu.texteDefaite();
        }
    }


}