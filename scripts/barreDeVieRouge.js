export default class BarreDeVie extends createjs.Shape {

    constructor(laVie){
        super();
        this.afficherInterface();

        this.vie = laVie;

        this.afficherInterface();
    }

    afficherInterface(){
        this.graphics
            .beginFill("red")
            .drawRect(0, 0, this.vie*5, 5);

        this.cache(0, 0, this.vie*5, 5);
    }

    updateMonCache(vieCourante){

        this.graphics
            .beginFill("red")
            .drawRect(0, 0, vieCourante*5, 5);

        this.cache(0, 0, vieCourante*5, 5);
    }
}