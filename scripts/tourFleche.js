export default class tourFleche extends createjs.Shape {

    constructor(leJeu, tour){
        super();
        this.afficherInterface();
        this.indexSort = 1;
        this.tour = tour;
        this.jeu = leJeu;

        this.ecouteurAlterner = this.sortAlterner.bind(this);
        window.addEventListener("keydown", this.ecouteurAlterner)
    }

    afficherInterface(){
        this.graphics
            .setStrokeStyle(5, "round")
            .beginStroke("white")
            .beginFill("black")
            .moveTo(20, 0)
            .lineTo(-20, -20)
            .lineTo(-20, 20)
            .lineTo(20, 0);

        this.cache(-50, -50, 110, 80);

    }

    sortAlterner(e){
        if(this.tour === true)
        {
            if(this.y === 75 && (e.key === "s" || e.key === "S"))
            {
                createjs.Sound.play("select", {"volume": 1});
                this.y = 10;
                this.indexSort = 1;
            }
            else if (this.y === 10 && (e.key === "s" || e.key === "S"))
            {
                createjs.Sound.play("select", {"volume": 1});
                this.y = 75;
                this.indexSort = 2;
            }
            if(e.key === "w" || e.key === "W")
            {
                createjs.Sound.play("select", {"volume": 1});
                this.detruire();
            }
        }
        else if(this.tour === false)
        {
            if(this.y === 75 && (e.key === "j" || e.key === "J"))
            {
                createjs.Sound.play("select", {"volume": 1});
                this.y = 10;
                this.indexSort = 1;
            }
            else if (this.y === 10 && (e.key === "j" || e.key === "J"))
            {
                createjs.Sound.play("select", {"volume": 1});
                this.y = 75;
                this.indexSort = 2;
            }
            if(e.key === "q" || e.key === "Q")
            {
                createjs.Sound.play("select", {"volume": 1});
                this.detruire();
            }
        }
    }

    detruire(){
        this.jeu.choisirCible(this.indexSort, this.tour);
        window.removeEventListener("keydown", this.ecouteurAlterner);
    }

}