export default class AttackSkeleton extends createjs.Sprite {

    constructor(chargeur, conteneurEnnemis = new createjs.Container, leJeu) {

        super(chargeur.getResult("skeletonAttack"));

        this.gotoAndPlay("Fx_effect6");

        createjs.Sound.play("sword", {"volume": 0.4});

        this.addEventListener("animationend", this.detruire.bind(this));

        this.conteneurEnnemis = conteneurEnnemis;
        this.chargeur = chargeur;

        this.leJeu = leJeu;
    }

    detruire(){


        this.parent.removeChild(this);

    }

}