
export default class FlecheVerte extends createjs.Sprite {

    constructor(chargeur, jeu, ennemies, indexSort, tour) {

        super(chargeur.getResult("vie"));

        this.gotoAndPlay("Fx_effect18");
        this.tour = tour;

        this.ConteneursEnnemis = ennemies;

        this.indexEnnemi = 0;

        this.indexSort = indexSort;

        this.scaleX = 1.5;
        this.scaleY = 1.5;
        this.jeu = jeu;
        this.monIndex = 0;
        this.x = (this.ConteneursEnnemis.getChildAt(this.monIndex).x - this.ConteneursEnnemis.getChildAt(this.monIndex).x/8.5)  - this.ConteneursEnnemis.getChildAt(this.monIndex).ajustementX;
        this.y = (this.ConteneursEnnemis.getChildAt(this.monIndex).y - 20)  + this.ConteneursEnnemis.getChildAt(this.monIndex).ajustementY;

        this.monEcouteur = this.switchR.bind(this);
        window.addEventListener("keydown", this.monEcouteur)
    }

        switchR(e){

        if(this.tour === true)
        {
            if(e.key === "s" || e.key === "S")
            {
                createjs.Sound.play("select", {"volume": 1});
                this.monIndex++;
                console.log(this.monIndex);
                if(this.monIndex >= this.ConteneursEnnemis.getNumChildren())
                {
                    this.monIndex = 0;
                    this.x = (this.ConteneursEnnemis.getChildAt(this.monIndex).x - this.ConteneursEnnemis.getChildAt(this.monIndex).x/8.5) - this.ConteneursEnnemis.getChildAt(this.monIndex).ajustementX;
                    this.y = (this.ConteneursEnnemis.getChildAt(this.monIndex).y - 20) + this.ConteneursEnnemis.getChildAt(this.monIndex).ajustementY;
                    this.indexEnnemi = this.monIndex;
                }
                else
                {

                    this.x = (this.ConteneursEnnemis.getChildAt(this.monIndex).x - this.ConteneursEnnemis.getChildAt(this.monIndex).x/8.5) - this.ConteneursEnnemis.getChildAt(this.monIndex).ajustementX;
                    this.y = (this.ConteneursEnnemis.getChildAt(this.monIndex).y - 20) + this.ConteneursEnnemis.getChildAt(this.monIndex).ajustementY;

                    this.indexEnnemi = this.monIndex;
                }

            }

            if (e.key === "w" || e.key === "W"){
                this.jeu.faireAttaque(this.indexEnnemi, this.indexSort);
                window.removeEventListener("keydown", this.monEcouteur);
                this.parent.removeChild(this);
            }
        }
        else if(this.tour === false)
        {
            if(e.key === "j" || e.key === "J")
            {
                createjs.Sound.play("select", {"volume": 1});
                this.monIndex++;
                console.log(this.monIndex);
                if(this.monIndex >= this.ConteneursEnnemis.getNumChildren())
                {
                    this.monIndex = 0;
                    this.x = (this.ConteneursEnnemis.getChildAt(this.monIndex).x - this.ConteneursEnnemis.getChildAt(this.monIndex).x/8.5) - this.ConteneursEnnemis.getChildAt(this.monIndex).ajustementX;
                    this.y = (this.ConteneursEnnemis.getChildAt(this.monIndex).y - 20) + this.ConteneursEnnemis.getChildAt(this.monIndex).ajustementY;
                    this.indexEnnemi = this.monIndex;
                }
                else
                {

                    this.x = (this.ConteneursEnnemis.getChildAt(this.monIndex).x - this.ConteneursEnnemis.getChildAt(this.monIndex).x/8.5) - this.ConteneursEnnemis.getChildAt(this.monIndex).ajustementX;
                    this.y = (this.ConteneursEnnemis.getChildAt(this.monIndex).y - 20) + this.ConteneursEnnemis.getChildAt(this.monIndex).ajustementY;

                    this.indexEnnemi = this.monIndex;
                }

            }

            if (e.key === "q" || e.key === "Q"){
                this.jeu.faireAttaque(this.indexEnnemi, this.indexSort);
                window.removeEventListener("keydown", this.monEcouteur);
                this.parent.removeChild(this);
            }
        }

        }

}