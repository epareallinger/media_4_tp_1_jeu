import Archer from './archer.js';
import Healer from './healer.js';
import Skeleton from './skeleton.js';
import Chauvesourie from './chauvesourie.js';
import Dragon from './dragon.js';

import tourArcher from "./tourFleche.js";
import FlecheVerte from "./FlecheVerte.js";


export default class Jeu {

  constructor() {

    // Référence au canevas dans le DOM
    this.canvas = document.querySelector("canvas");

    // Paramètres modifiable du jeu
    this.params = {
      cadence: 60,
      texteValue: {
            format: "32px \"Minecraft Ok\"",
            couleur: "white",
            couleur2: "#000",
          format2: "110px \"Minecraft Ok\"",
          couleurV: "#FFFB4C",
          couleurD: "#CC0800"

        },
    };

    this.initialiser();

      this.indexEnnemi = 0;

      this.enCours = true;

      this.tour = false;
  }


  //-----------------Charger fonts-----------------//
  initialiser() {

    document.fonts.load(this.params.texteValue.format).then(this.chargerRessources.bind(this));

  }

    //-----------------Charger Manifest-----------------//
  chargerRessources(){

      this.chargeur = new createjs.LoadQueue();
      this.chargeur.installPlugin(createjs.Sound);
      this.chargeur.loadManifest("ressources/manifest.json");
      console.log("Ready");



      // Ajustement du canevas à la taille de la fenêtre
      this.canvas.width = 1280;
      this.canvas.height = 800;

      // Préparation du StageGL
      this.stage = new createjs.StageGL(this.canvas);
      this.stage.setClearColor("black");

      // Préparation du Ticker
      createjs.Ticker.addEventListener("tick", e => this.stage.update(e));
      createjs.Ticker.timingMode = createjs.Ticker.RAF_SYNCHED;
      createjs.Ticker.framerate = this.params.cadence;

      this.chargeur.addEventListener('complete', this.debuter.bind(this));

  }


    //-----------------Cadran de secondes-----------------//
  cadran(){

      this.temps = 0;

      this.pointage = new createjs.Text(this.temps + " secondes", this.params.texteValue.format,this.params.texteValue.couleur);
      this.pointage.x = this.stage.canvas.width/2 + this.pointage.getBounds().width/2 - 120;
      this.pointage.y = 50;
      this.stage.addChild(this.pointage);
      this.pointage.cache(0,0,  this.pointage.getBounds().width,  this.pointage.getBounds().height);

      this.monInterval = setInterval(this.timer.bind(this), 1000);
  }

    //-----------------Augmenter le cadran à chaque secondes-----------------//
    timer(){

        this.temps++;
        this.pointage.text = this.temps + " secondes";

        this.pointage.uncache();
        this.pointage.cache(0,0, this.pointage.getBounds().width, this.pointage.getBounds().height);

    }



    //-----------------Partir la musique et appeler les fonctions-----------------//

  debuter() {

      let musique = createjs.Sound.play("musique", {"volume": 0.5, "loop": -1});

      document.addEventListener("visibilitychange", function(){
        musique.paused = !musique.paused;
        createjs.Ticker.paused = !createjs.Ticker.paused;
      });

      this.instructions();
      this.ajouterDecors();

  }

    //-----------------Afficher les records et les instructions-----------------//

  instructions(){


      //this.monEcouteurDebuter = this.debuterJeu.bind(this);
      //window.addEventListener("keydown", this.monEcouteurDebuter);

      this.fenetre = document.getElementById("fenetre");
      this.fenetre.style.display = "block";

      this.nomAttaquant = document.getElementById("attaquant");
      this.nomDefenseur = document.getElementById("defenseur");
      this.bouton = document.querySelector("#fenetre button");

      this.nomAttaquant.focus();

      this.ecouteur =e=> e.stopPropagation();

      this.nomAttaquant.addEventListener("keydown", this.ecouteur);
      this.nomDefenseur.addEventListener("keydown", this.ecouteur);

      this.bouton.addEventListener("click", ()=>{

          if(this.nomDefenseur.value.length < 1 || this.nomAttaquant.value.length < 1){
              alert("Veuillez remplir les cases!");
              return;
          }

          this.attaquantNom = this.nomAttaquant.value;
          this.healerNom = this.nomDefenseur.value;
          this.fenetre.style.display = "none";

          this.stage.removeChild(this.menu);
          this.stage.removeChild(this.recordTemps);
          this.stage.removeChild(this.recordTeam);



          this.nomAttaquant.removeEventListener("keydown", this.ecouteur);
          this.nomDefenseur.removeEventListener("keydown", this.ecouteur);
          this.debuterJeu()
      });

      this.menu = new createjs.Bitmap(this.chargeur.getResult("menu"), true);
      this.stage.addChildAt(this.menu);

      this.menu.x = this.stage.canvas.width / 2 - 416;
      this.menu.y = this.stage.canvas.height / 2 - 277.5;

      if(localStorage.getItem("records") !== null){
          let mesRecords = JSON.parse(localStorage.getItem('records'));
          this.recordTemps = new createjs.Text(mesRecords.equipe.temps + " secondes", this.params.texteValue.format, this.params.texteValue.couleur2);
          this.recordTemps.cache(0, 0, this.recordTemps.getBounds().width = 500, this.recordTemps.getBounds().height = 200);
          this.recordTemps.x = this.stage.canvas.width / 2 - 100;
          this.recordTemps.y = this.stage.canvas.height / 2 + 150;
          this.stage.addChild(this.recordTemps);

          this.recordTeam = new createjs.Text(mesRecords.equipe.nom1 + " et " + mesRecords.equipe.nom2, this.params.texteValue.format, this.params.texteValue.couleur2);
          this.recordTeam.cache(0, 0, this.recordTeam.getBounds().width = 500, this.recordTeam.getBounds().height = 200);
          this.recordTeam.x = this.stage.canvas.width / 2 - 100;
          this.recordTeam.y = this.stage.canvas.height / 2 + 200;
          this.stage.addChild(this.recordTeam);

      }
      else{
          this.recordTemps = new createjs.Text("Aucun temps", this.params.texteValue.format, this.params.texteValue.couleur2);
          this.recordTemps.cache(0, 0, this.recordTemps.getBounds().width = 500, this.recordTemps.getBounds().height = 200);
          this.recordTemps.x = this.stage.canvas.width / 2 - 100;
          this.recordTemps.y = this.stage.canvas.height / 2 + 150;
          this.stage.addChild(this.recordTemps);
      }

  }


    //-----------------Local storage des meilleurs joueurs-----------------//

    faireStorage(){

        this.records = JSON.parse(localStorage.getItem('records'));

        if (!this.records) {

            this.records= {

                equipe: {

                    nom1: this.attaquantNom,
                    nom2: this.healerNom,
                    temps: parseInt(this.pointage.text)

                },

            }

        }

        if (parseInt(this.pointage.text) < this.records.equipe.temps){

            this.records.equipe.nom1 = (this.attaquantNom);
            this.records.equipe.nom2 = (this.healerNom);
            this.records.equipe.temps = parseInt(this.pointage.text);
        }

        localStorage.setItem('records', JSON.stringify(this.records));




    }


    //-----------------Debuter le jeu-----------------//

  debuterJeu(){
      this.cadran();
      this.ajouterHealer();
      this.ajouterArcher();

      this.ajouterEnnemi();
      //window.removeEventListener("keydown", this.monEcouteurDebuter)
  }

    //-----------------Si on gagne-----------------//

    texteVictoire(){

        this.enCours = false;

        this.stage.removeChild(this.healer.barreDeVie);
        this.stage.removeChild(this.attaquant.barreDeVie);
        this.stage.removeChild(this.conteneurEnnemis);
        this.stage.removeChild(this.attaquant);
        this.stage.removeChild(this.healer);


        this.textegagne = new createjs.Text("Victoire!", this.params.texteValue.format2, this.params.texteValue.couleurV);
        this.textegagne.cache(0, 0, this.textegagne.getBounds().width = 500, this.textegagne.getBounds().height = 200);
        this.textegagne.x = this.stage.canvas.width / 2 - 250;
        this.textegagne.y = this.stage.canvas.height / 2 - 50;
        this.stage.addChild(this.textegagne);

        this.pointage.y += 400;
        this.pointage.uncache();
        this.pointage.cache(0,0, this.pointage.getBounds().width, this.pointage.getBounds().height);

        clearInterval(this.monInterval);

        this.faireStorage();

        window.setTimeout(this.fRestart, 3000);

    }

    //-----------------Si on perds-----------------//
    texteDefaite(){

        this.enCours = false;

        this.tour = false;
        this.stage.removeChild(this.healer.barreDeVie);
        this.stage.removeChild(this.attaquant.barreDeVie);
        this.stage.removeChild(this.attaquant);
        this.stage.removeChild(this.healer);

        this.conteneurEnnemis.removeAllChildren();
        this.stage.removeChild(this.ennemie[0].barreDeVie, this.ennemie[1].barreDeVie, this.ennemie[2].barreDeVie);


        this.textegagne = new createjs.Text("Defaite...", this.params.texteValue.format2, this.params.texteValue.couleurD);
        this.textegagne.cache(0, 0, this.textegagne.getBounds().width = 500, this.textegagne.getBounds().height = 200);
        this.textegagne.x = this.stage.canvas.width / 2 - 250;
        this.textegagne.y = this.stage.canvas.height / 2 - 50;
        this.stage.addChild(this.textegagne);

        this.pointage.y += 400;
        this.pointage.uncache();
        this.pointage.cache(0,0, this.pointage.getBounds().width, this.pointage.getBounds().height);



        clearInterval(this.monInterval);

        if(this.dragon !== undefined)
        {
            this.stage.removeChild(this.dragon);
            this.stage.removeChild(this.dragon.barreDeVie);
        }

        window.setTimeout(this.fRestart, 3000);

    }

    //-----------------Fonction pour reload la page-----------------//

    fRestart() {

        location.reload();

    }

    //-----------------Ajouter l'archer au stage-----------------//

  ajouterArcher(){


      this.attaquant = new Archer(this.chargeur, this.conteneurEnnemis, this);
      this.stage.addChild(this.attaquant);
      this.attaquant.x = 350;
      this.attaquant.y = 300;

      this.attaquant.afficherVie(this.attaquant.x, this.attaquant.y);

  }

    //-----------------Ajouter le healer au stage-----------------//

    ajouterHealer(){
        this.conteneurEnnemis = new createjs.Container();
        this.stage.addChild(this.conteneurEnnemis);

        this.healer = new Healer(this.chargeur, this.conteneurEnnemis, this);
        this.stage.addChild(this.healer);
        this.healer.x = 300;
        this.healer.y = 400;

        this.healer.afficherVie(this.healer.x, this.healer.y);



    }
    //-----------------Ajouter le dragon au stage----------------//

    ajouterDragon(){

        if(this.enCours === true){

            this.dragon = new Dragon(this.chargeur, this.conteneurEnnemis, this);
            this.conteneurEnnemis.addChild(this.dragon);
            setTimeout(function(){
                this.dragon.afficherVie(this.dragon.x, this.dragon.y);
                this.tourArcher()
            }.bind(this), 3500);


        }


    }
    //-----------------Ajouter les ennemis du container-----------------//

    ajouterEnnemi() {

        this.ennemie = [
            new Skeleton(this.chargeur, this.conteneurEnnemis, this),
            new Chauvesourie(this.chargeur, this.conteneurEnnemis, this),
            new Skeleton(this.chargeur, this.conteneurEnnemis, this)
        ];

        this.ennemie[0].y = 250;
        this.ennemie[0].x = 800;



        this.ennemie[1].y = 350;
        this.ennemie[1].x = 750;

        this.ennemie[2].y = 425;
        this.ennemie[2].x = 825;

        this.ennemie[2].scaleX = -1;

        this.ennemie[0].scaleX = -1;
        this.ennemie[1].scaleX = -1;


        this.conteneurEnnemis.addChild(this.ennemie[0], this.ennemie[1], this.ennemie[2]);

        this.ennemie[0].afficherVie(this.ennemie[0].x, this.ennemie[0].y);
        this.ennemie[1].afficherVie(this.ennemie[1].x, this.ennemie[1].y);
        this.ennemie[2].afficherVie(this.ennemie[2].x, this.ennemie[2].y);

        this.tourArcher()
    }



    //-----------------------------------------------------------------------------------//



    //-----------------Ajouter le background-----------------//

  ajouterDecors(){

      let decor1 = new createjs.Bitmap(this.chargeur.getResult("plaines"), true);
      this.stage.addChildAt(decor1,0);

    }

    //-----------------À appeler lorsque c'est le tour de l'archer-----------------//

    tourArcher(){
        this.nombreEnnemis = this.conteneurEnnemis.getNumChildren();
        if(this.nombreEnnemis > 0)
        {
            console.log("Tour Archer");
            this.tourFleche = new tourArcher(this, this.tour);
            this.conteneurSortsArcher = new createjs.Container();
            this.stage.addChild(this.conteneurSortsArcher);

            this.conteneurSortsArcher.x = 150;
            this.conteneurSortsArcher.y = 650;

            this.conteneurSortsArcher.addChild(this.tourFleche);

            this.sortArcher1 = new createjs.Text("Fleche Explosive", this.params.texteValue.format, this.params.texteValue.couleur);
            this.conteneurSortsArcher.addChild(this.sortArcher1);
            this.sortArcher1.x = this.conteneurSortsArcher.x - 100;
            this.sortArcher1.cache(0,0, this.sortArcher1.getBounds().width + 200, this.sortArcher1.getBounds().height + 200);

            this.sortArcher2 = new createjs.Text("Tir Precis", this.params.texteValue.format, this.params.texteValue.couleur);
            this.conteneurSortsArcher.addChild(this.sortArcher2);
            this.sortArcher2.x = this.sortArcher1.x;
            this.sortArcher2.y = this.sortArcher1.y + 60;
            this.sortArcher2.cache(0,0, this.sortArcher2.getBounds().width + 200, this.sortArcher2.getBounds().height + 200);

            this.nomArcher = new createjs.Text(this.attaquantNom, this.params.texteValue.format, this.params.texteValue.couleur);
            this.conteneurSortsArcher.addChild(this.nomArcher);
            this.nomArcher.x = this.tourFleche.x - 15;
            this.nomArcher.y = this.sortArcher1.y - 60;
            this.nomArcher.cache(0,0, this.nomArcher.getBounds().width + 200, this.nomArcher.getBounds().height + 200);

            this.tourFleche.y = 10;
        }
        else
        {
            this.ajouterDragon();
            console.log("Attends");
        }
    }

//-----------------À appeler lorsque c'est le tour du healer-----------------//

    tourHealer(){

      this.nombreEnnemis = this.conteneurEnnemis.getNumChildren();
      if(this.nombreEnnemis > 0)
      {
          console.log("Tour Healer");
          this.tourFleche = new tourArcher(this, this.tour);
          this.conteneurSortsHealer = new createjs.Container();
          this.stage.addChild(this.conteneurSortsHealer);

          this.conteneurSortsHealer.x = 150;
          this.conteneurSortsHealer.y = 650;

          this.conteneurSortsHealer.addChild(this.tourFleche);

          this.sortHealer1 = new createjs.Text("Tempete de vent", this.params.texteValue.format, this.params.texteValue.couleur);
          this.conteneurSortsHealer.addChild(this.sortHealer1);
          this.sortHealer1.x = this.conteneurSortsHealer.x - 100;
          this.sortHealer1.cache(0,0, this.sortHealer1.getBounds().width + 200, this.sortHealer1.getBounds().height + 200);

          this.sortHealer2 = new createjs.Text("Vol de vie", this.params.texteValue.format, this.params.texteValue.couleur);
          this.conteneurSortsHealer.addChild(this.sortHealer2);
          this.sortHealer2.x = this.sortHealer1.x;
          this.sortHealer2.y = this.sortHealer1.y + 60;
          this.sortHealer2.cache(0,0, this.sortHealer2.getBounds().width + 200, this.sortHealer2.getBounds().height + 200);

          this.nomHealer = new createjs.Text(this.healerNom, this.params.texteValue.format, this.params.texteValue.couleur);
          this.conteneurSortsHealer.addChild(this.nomHealer);
          this.nomHealer.x = this.tourFleche.x - 15;
          this.nomHealer.y = this.sortHealer1.y - 60;
          this.nomHealer.cache(0,0, this.nomHealer.getBounds().width + 200, this.nomHealer.getBounds().height + 200);

          this.tourFleche.y = 10;
      }
      else
      {
          this.ajouterDragon();
          console.log("Attends");
      }
    }

    //-----------------Faire apparaître la flèche pour choisir la cible-----------------//

    choisirCible(indexSort, tour) {


          this.flecheChoisirEnnemi = new FlecheVerte(this.chargeur, this, this.conteneurEnnemis, indexSort, tour);
          this.stage.addChild(this.flecheChoisirEnnemi);


          if (this.tour === false) {
              this.stage.removeChild(this.conteneurSortsArcher);
          }
          else if(this.tour === true)
          {
              this.stage.removeChild(this.conteneurSortsHealer);
          }
    }

//-----------------Faire apparaître l'attaque dépendemment du tour à qui-----------------//


    faireAttaque(indexEnnemi, indexSort)
        {
            //Si c'est le tour de l'Archer,
            if(this.tour === false){

                if (indexSort === 1) {
                    this.attaquant.attack1(indexEnnemi);
                }
                else if (indexSort === 2) {
                    this.attaquant.attack2(indexEnnemi);
                }

                setTimeout(function(){
                    if(this.conteneurEnnemis.getNumChildren() !== 0)
                    {
                        this.tour = true;
                        this.tourHealer();
                    }
                    else{
                        if(this.enCours === true)
                        {
                            this.ajouterDragon()
                        }


                    }
                }.bind(this), 2000)
            }
            else if(this.tour === true)
            //Si c'est le tour du Healer,
            {

                if (indexSort === 1) {
                    this.healer.attack1(indexEnnemi);
                }
                else if (indexSort === 2) {
                    this.healer.attack2(indexEnnemi);
                }
                this.tour = false;

                    setTimeout(this.tourEnnemis.bind(this), 2000);

            }
        }

    //-----------------Faire le tour de chaque ennemi-----------------//

     tourEnnemis(){
      let nombreEnnemiTour = 0;
      setTimeout(function(){
          if(this.conteneurEnnemis.getNumChildren() === 0)
          {
              console.log("Dragon");
              this.tour = false;
              setTimeout(this.tourArcher.bind(this), 1000);
              return;
          }
          console.log(this.conteneurEnnemis.getNumChildren());
          for (let i = 0; i < this.conteneurEnnemis.getNumChildren(); i++) {
              (function(i) {
                  setTimeout(function() {
                      nombreEnnemiTour++;
                      this.conteneurEnnemis.getChildAt(i).attack(this.healer, this.attaquant);

                      if(nombreEnnemiTour === this.conteneurEnnemis.getNumChildren() && this.enCours === true && this.conteneurEnnemis.getNumChildren !== 0)
                      {
                          console.log("On continue");
                          setTimeout(this.tourArcher.bind(this), 2000);
                      }
                      }.bind(this), 1200 * i);
              }.bind(this))(i);
          }
      }.bind(this), 1000);


     }



}
