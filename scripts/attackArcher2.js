export default class AttackArcher2 extends createjs.Sprite {

    constructor(chargeur, conteneurEnnemis = new createjs.Container , valeurX, valeurY, indexEnnemi) {

        super(chargeur.getResult("archerAttack2"));

        this.gotoAndPlay("Fx_effect16");

        createjs.Sound.play("fleche", {"volume": 1});

        this.conteneurEnnemis = conteneurEnnemis;

        this.valeurX = valeurX;
        this.valeurY = valeurY;

        this.x = this.valeurX;
        this.y = this.valeurY + 115;

        this.rotation = -65;

        this.indexEnnemi = indexEnnemi;

        this.ennemiChoix()


    }

    ennemiChoix(){

        createjs.Tween
                 .get(this, createjs.Ease.linear)
                 .to({x: this.conteneurEnnemis.getChildAt(this.indexEnnemi).x - 220 - this.conteneurEnnemis.getChildAt(this.indexEnnemi).ajustementX, y: this.conteneurEnnemis.getChildAt(this.indexEnnemi).y + 100 + this.conteneurEnnemis.getChildAt(this.indexEnnemi).ajustementY}, 400)
                 .call(this.detruire.bind(this));

    }

    detruire(){
        this.parent.removeChild(this);
             createjs.Tween.removeTweens(this);


        let dommageAttaque = Math.random()*2 + 2;
        let dommageAttaqueArrondi = Math.round(dommageAttaque);
        this.conteneurEnnemis.getChildAt(this.indexEnnemi).vie -= dommageAttaqueArrondi;
        this.conteneurEnnemis.getChildAt(this.indexEnnemi).hit();
        this.conteneurEnnemis.getChildAt(this.indexEnnemi).die();
    }

}