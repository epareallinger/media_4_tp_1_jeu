
export default class AttackChauveSouris extends createjs.Sprite{

    constructor(chargeur, conteneurEnnemis = new createjs.Container, leJeu) {

        super(chargeur.getResult("chauvesourieAttack"));

        this.gotoAndPlay("Fx_effect12");

        createjs.Sound.play("feu", {"volume": 0.7});

        this.addEventListener("animationend", this.detruire.bind(this));

        this.chargeur = chargeur;
        this.scaleX = 1.5;
        this.scaleY = 1.5;

        this.conteneurEnnemis = conteneurEnnemis;

        this.leJeu = leJeu;

    }

    detruire(){


        this.parent.removeChild(this);

    }

    animation(){

       //this.conteneurEnnemis.getChildAt(0).hit1();
       //this.conteneurEnnemis.getChildAt(1).hit2();
       //this.conteneurEnnemis.getChildAt(2).hit3();

    }

}