

export default class AttackArcher1 extends createjs.Sprite {

    constructor(chargeur, conteneurEnnemis = new createjs.Container, valeurX, valeurY, indexEnnemi) {

        super(chargeur.getResult("archerAttack1"));

        this.gotoAndPlay("Fx_effect16");

        createjs.Sound.play("flecheExplo", {"volume": 0.2});

        this.conteneurEnnemis = conteneurEnnemis;
        this.indexEnnemi = indexEnnemi;

        this.valeurX = valeurX;
        this.valeurY = valeurY;

        this.x = this.valeurX;
        this.y = this.valeurY + 115;

        this.scaleX = 2;
        this.scaleY = 2;


        this.ennemiHit()
    }

    ennemiHit(){


        setTimeout(this.detruire3.bind(this), 900);
        setTimeout(this.attaque.bind(this), 300);

    }

    detruire3(){

        this.parent.removeChild(this);

    }


    attaque(){
        let n = 0;

        this.conteneurEnnemis.getChildAt(this.indexEnnemi).vie -= 1;
        this.conteneurEnnemis.getChildAt(this.indexEnnemi).hit();

        while (n < this.conteneurEnnemis.getNumChildren()){
            let dommageAttaque = Math.random() + 1;
            let dommageAttaqueArrondi = Math.round(dommageAttaque);
            this.conteneurEnnemis.getChildAt(n).vie -= dommageAttaqueArrondi;
            this.conteneurEnnemis.getChildAt(n).hit();
            this.conteneurEnnemis.getChildAt(n).die();
            n++;
        }


    }
}