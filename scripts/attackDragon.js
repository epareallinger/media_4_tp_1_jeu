
export default class AttackDragon extends createjs.Sprite{

    constructor(chargeur, conteneurEnnemis = new createjs.Container, leJeu, dragon, healer, attaquant) {

        super(chargeur.getResult("dragonAttack"));

        this.gotoAndPlay("Fx_effect13_2");

        this.animation.bind(this);
        //this.addEventListener("animationend", this.detruire.bind(this));

        this.chargeur = chargeur;
        this.scaleX = 1;
        this.scaleY = 1;
        this.dragon = dragon;
        this.healer = healer;
        this.attaquant = attaquant;

        this.x = 600;
        this.y = this.dragon.y + 250;
        this.animation();

        this.conteneurEnnemis = conteneurEnnemis;




        this.leJeu = leJeu;
    }


    detruire(){


        this.parent.removeChild(this);


    }

    animation(){
        console.log(this);
        createjs.Tween
            .get(this, createjs.Ease.linear)
            .to({x: ((this.attaquant.x + this.attaquant.getBounds().width/2) + (this.healer.x + this.healer.getBounds().width/2))/2 - 30,y:((this.attaquant.y + this.attaquant.getBounds().height - 18) + (this.healer.y + this.healer.getBounds().height - 18))/2 - 40}, 600)
            .call(this.animation2.bind(this))

    }

    animation2(){
        this.gotoAndPlay("Fx_effect13");
        createjs.Sound.play("dragExplo", {"volume": 0.7});
        this.addEventListener("animationend", this.detruire.bind(this));

        let dommageAttaque = Math.random()*4 + 2;
        let dommageAttaqueArrondi = Math.round(dommageAttaque);

        this.healer.vie -= dommageAttaqueArrondi;
        this.healer.hit();
        this.healer.die();

        this.attaquant.vie -= dommageAttaqueArrondi;
        this.attaquant.hit();
        this.attaquant.die();
    }

}